use std::process::Command;

pub fn build(file: &str, name: &str) {
    match Command::new("clang").args(&["-o", name, file]).spawn() {
        Ok(_) => (),
        Err(e) => {
            eprintln!("Error building: {}", e);
        }
    }
}

pub fn run(file: &str) {
    match Command::new("lli").args(&[file]).spawn() {
        Ok(_) => (),
        Err(e) => {
            eprintln!("Error running file: {}", e);
        }
    }
}
