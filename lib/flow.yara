rule If_eq_i32
{
	meta:
	ir = "%$ = load i32, i32* %#, align 4\n\t%$ = icmp eq i32 %^2, #"
	location = "main"
	start = "(:["
	end = "):]"

	strings:
	$ = /if \([a-zA-Z]*\) eq \[[0-9]*\]/

	condition:
	any of them
}

rule If_neq_i32
{
	meta:
	ir = "%$ = load i32, i32* %#, align 4\n\t%$ = icmp ne i32 %^2, #"
	location = "main"
	start = "(:["
	end = "):]"

	strings:
	$ = /if \([a-zA-Z]*\) neq \[[0-9]*\]/

	condition:
	any of them
}

rule If_slt_i32
{
	meta:
	ir = "%$ = load i32, i32* %#, align 4\n\t%$ = icmp slt i32 %^2, #"
	location = "main"
	start = "(:["
	end = "):]"

	strings:
	$ = /if \([a-zA-Z]*\) lt \[[0-9]*\]/

	condition:
	any of them
}

rule If_sgt_i32
{
	meta:
	ir = "%$ = load i32, i32* %#, align 4\n\t%$ = icmp sgt i32 %^2, #"
	location = "main"
	start = "(:["
	end = "):]"

	strings:
	$ = /if \([a-zA-Z]*\) gt \[[0-9]*\]/

	condition:
	any of them
}

rule If_then 
{
	meta:
	ir = "br i1 %^1, label %$, label %#\n^1:"
	location = "main"
	start = "("
	end = ")"

	strings:
	$ = /then => \([a-zA-Z]*\)/

	condition:
	any of them
}

rule If_end
{
	meta:
	ir = "br label %#\n#:"
	location = "main"
	start = "(:("
	end = "):)"

	strings:
	$ = /end \([a-zA-Z]*\)/

	condition:
	any of them
}
