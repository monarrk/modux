rule Inline_ir
{
	meta:
	ir = "#"
	location = "main"
	start = "["
	end = "]"

	strings:
	$ = /unsafe inline \[[a-zA-Z0-9\s\S]*\]/

	condition:
	any of them
}

rule Inline_ir_head
{
	meta:
	ir = "#"
	location = "header"
	start = "["
	end = "]"

	strings:
	$ = /unsafe inline header \[[a-zA-Z0-9\s\S]*\]/

	condition:
	any of them
}
