rule Define_fn_void
{
	meta:
	ir = "define dso_local void @#() {#\tret void\n}"
	location = "header"
	start = "(:{"
	end = "):}"

	strings:
	$ = /ir fn \([a-zA-Z]*\) -> void \{[a-zA-Z0-9\s\S]*\}/

	condition:
	any of them
}
