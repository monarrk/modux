include "io.yara"
include "flow.yara"
include "ir.yara"
include "fn.yara"

rule Def_i32
{
	meta:
	ir = "%# = alloca i32, align 4"
	location = "main"
	starts = "("
	ends = ")"

	strings:
	$ = /var \([a-zA-Z]*\) i32/

	condition:
	any of them
}

rule Mut_i32
{
	meta:
	ir = "store i32 #, i32* %#, align 4"
	location = "main"
	starts = "[:("
	ends = "]:)"

	strings:
	$store = /i32 \[[0-9]*\] -> \([a-zA-Z]*\)/

	condition:
	$store and Def_i32
}

rule Def_arbitrary
{
	meta:
	ir = "%# = alloca #, align 4"
	location = "main"
	starts = "(:{"
	ends = "):}"

	strings:
	$ = /var \([a-zA-Z]*\) \{[a-zA-Z0-9]*\}/

	condition:
	any of them
}

rule Mut_arbitrary
{
	meta:
	ir = "store # #, #* %#, align 4"
	location = "main"
	starts = "{:[:{:("
	ends = "}:]:}:)"

	strings:
	$ = /\{[a-zA-Z0-9]*\} \[[0-9]*\] -> \([a-zA-Z]*\)/

	condition:
	any of them
}

rule Def_char
{
	meta:
	ir = "%# = alloca i8, align 1"
	location = "main"
	starts = "("
	ends = ")"

	strings:
	$ = /var \([a-zA-Z]*\) i8/

	condition:
	any of them
}

rule Mut_char
{
	meta:
	ir = "store i8 #, i8* %#, align 1"
	location = "main"
	starts = "[:("
	ends = "]:)"

	strings:
	$store = /i8 \[[0-9]*\] -> \([a-zA-Z]*\)/

	condition:
	$store and Def_char
}

rule Def_cast_ptr
{
	meta:
	ir = "%# = load #, #* %#, align 4"
	location = "main"
	starts = "(:{:{:["
	ends = "):}:}:]"

	strings:
	$ = /pointer \{[a-zA-Z0-9]*\} \[[a-zA-Z]*\] -> \([a-zA-Z]*\)/

	condition:
	any of them
}

rule Def_str
{
	meta:
	ir = "@.# = private unnamed_addr constant [# x i8] c\"#\\00\", align 1"
	location = "header"
	start = "(:{:["
	ends = "):}:]"

	strings:
	$ = /global str \[[a-zA-Z0-9\s]*\]\{[0-9]*\} -> \([a-zA-Z]*\)/

	condition:
	any of them
}

rule Def_str_ln
{
	meta:
	ir = "@.# = private unnamed_addr constant [# x i8] c\"#\\0A\\00\", align 1"
	location = "header"
	start = "(:{:["
	end = "):}:]"

	strings:
	$ = /global strln \[[a-zA-Z0-9\s]*\]\{[0-9]*\} -> \([a-zA-Z]*\)/

	condition:
	any of them
}

rule Loop_start
{
	meta:
	ir = "br label %#\n#:"
	location = "main"
	start = "(:("
	end = "):)"

	strings:
	$ = /loop \([a-zA-Z]*\) do/

	condition:
	any of them
}

rule Loop_end
{
	meta:
	ir = "br label %#"
	location = "main"
	start = "("
	end = ")"

	strings:
	$ = /done \([a-zA-Z]*\)/

	condition:
	any of them
}

rule Def_comment
{
	meta:
	ir = "; #"
	location = "main"
	start = "{"
	end = "}"

	strings:
	$ = /{- [a-zA-Z0-9\s]* -}/

	condition:
	any of them
}
