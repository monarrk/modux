rule Fn_define_puts
{
        meta:
	ir = "declare i32 @puts(i8* nocapture) nounwind"
	location = "header"

	strings:
	$ = "init puts"

	condition:
	any of them
}

rule Fn_puts_char
{
        meta:
        ir = "call i32 @puts(i8* %#)"
        location = "main"
        starts = "("
        ends = ")"

        strings:
         $a = /puts_char \([a-zA-Z]*\)/

        condition:
        $a and Fn_define_puts
}

rule Fn_puts_str
{
        meta:
        ir = "%$ = alloca i8*, align 8\n\tstore i8* getelementptr inbounds ([# x i8], [# x i8]* @.#, i64 0, i64 0), i8** %^1, align 8\n\t%$ = load i8*, i8** %^2, align 8\n\tcall i32 @puts(i8* %^1)"
        location = "main"
        starts = "{:{:("
        ends = "}:}:)"

        strings:
        $a = /puts \([a-zA-Z]*\)\{[0-9]*\}/

        condition:
        $a and Fn_define_puts
}

rule Def_stdin
{
	meta:
	ir = "%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, %struct._IO_codecvt*, %struct._IO_wide_data*, %struct._IO_FILE*, i8*, i64, i32, [20 x i8] }\n%struct._IO_codecvt = type opaque\n%struct._IO_marker = type opaque\n%struct._IO_wide_data = type opaque\n@stdin = external dso_local global %struct._IO_FILE*, align 8"
	location = "header"

	strings:
	$ = "init stdin"

	condition:
	any of them
}
