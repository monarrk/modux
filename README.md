# Modux
> *There is no language*

An experimental language with extremely modular syntax.

### Building
##### UNIX/UNIX-likes

```
# sys/install.sh
```

##### Windows:

```
% cargo build --release
```

And then from there you're on your own; I don't know how to use Windows.

### Examples
Because there is *technically* no syntax, this example is not really an example of the language itself but of the standard syntax. That being said, here's a 'hello world':

```
{- Anything that isn't regular syntax is a comment! -}

{- Add the `puts` function to the header -}
init puts

{- Strings have a static length defined with {[0-9]*} -}
global str [hello world]{12} -> (hello)
puts (hello){12}
```

### Defining syntax
Syntax is defined using [YARA](https://virustotal.github.io/yara/). To define a chunk of syntax, create a new rule, with strings for each pattern that represents that expression. Use metadata to define the LLVM IR that will be omitted, using `#` for placeholders for characters which will get sliced out of the strings. If you do use placeholders, define the starts and ends of the slice like so:

```
metadata:
	ir = "; test # and #
	starts = "(:["
	ends = "):]"
```

This will slice the string like `string[indexof('(')..indexof(')')]` and `string[indexof('[')..indexof(']')]`.

You can also use placeholder values for repeated operations. To define a placeholder, use `$`. To cycle back to old placeholders, use `^index`. For example:

```
metadata:
	ir = "; Here's a placeholder: $. Here's another: $. Here's the first placeholder: ^2"
```

### Examples
Go to `examples/`. In here, you will notice two files: `test.mx` and `rules.yara`. `rules.yara` contains all the rules that will be used to parse the file, and all this one does is import the standard syntax. `test.mx` is the code we will actually compile. When in this directory, you can simply run `modux test.mx`, which will scan the `rules.yara` file and compile to LLVM IR. If you wish to import a custom rules file, you can do so with the `-r` flag: `modux -r custom_rules.yara test.mx`.

### Disclaimers
- This language is a terrible idea
- This language should not be used for anything
- This language is a result of procrastination and adderrall
- Please don't look at my rust code; it is irredeemable
