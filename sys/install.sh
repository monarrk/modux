#!/bin/sh

alias cp="cp -v"

cargo build --release

if [[ -d "/usr/local/lib/modux" ]]; then
	rm -rf /usr/local/lib/modux
fi
cp ./target/release/modux /usr/local/bin
cp -r lib /usr/local/lib/modux
